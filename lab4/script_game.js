var yellowCheker = "https://upload.wikimedia.org/wikipedia/commons/8/82/Circle-yellow.svg";
var whiteCheker = "https://upload.wikimedia.org/wikipedia/commons/5/58/White_Circle.svg";
var blackCheker = "https://upload.wikimedia.org/wikipedia/commons/2/27/Disc_Plain_black.svg";

var whiteKing = "https://www.flaticon.com/svg/static/icons/svg/606/606032.svg";
var blackKing = "https://www.flaticon.com/svg/static/icons/svg/606/606114.svg";
var yellowKing = "https://www.flaticon.com/svg/static/icons/svg/864/864640.svg";

//проверяем, включен ли режим подсказки
var isCheked = false;

let characters = ["A", "B", "C", "D", "E", "F", "G", "H"]
//модели расстановки, которые нужно вывести при нажатии на кнопки
let boardBegin = [
	["", "b1", "", "b2", "", "b3", "", "b4"],
	["b5", "", "b6", "", "b7", "", "b8", ""],
	["", "b9", "", "b10", "", "b11", "", "b12"],
	["", "", "", "", "", "", "", ""],
	["", "", "", "", "", "", "", ""],
	["w12", "", "w11", "", "w10", "", "w9", ""],
	["", "w8", "", "w7", "", "w6", "", "w5"],
	["w4", "", "w3", "", "w2", "", "w1", ""]
];

let boardExample = [
	["", "b1", "", "", "", "", "", ""],
	["", "", "b4", "", "b5", "", "", ""],
	["", "", "", "", "", "", "", "b6"],
	["", "", "b3", "", "", "", "", ""],
	["", "", "", "", "", "w1", "", "w2"],
	["", "", "", "", "", "", "", ""],
	["", "", "", "", "", "", "", ""],
	["", "", "b2", "", "", "", "", ""]
];

var map = new Map();

//Model of chekers
let board = boardBegin;

// нажали на кнопку "Начало"
function begin() {
clearCurrentBoard();
copyElem(boardBegin);
}

// нажали на кнопку "Пример 1"
function example1() {
clearCurrentBoard();
var queen = map.get(boardExample[7][2]);
queen.src = blackKing;
copyElem(boardExample);
}

// приводим доску к начальному состоянию
function clearCurrentBoard() {
	for (let i = 0; i < board.length; i++) {
		for (let j = 0; j < board.length; j++) {
			if (board[i][j] != "") {
				var id = characters[j] + (board.length - i);
				var elem = document.getElementById(id);
				var img = document.getElementById(board[i][j]);
				map.set(board[i][j], img);
				if (board[i][j][0] == 'b') {
					img.src = blackCheker;	
				} else {
					img.src = whiteCheker;
				}
 				elem.removeChild(img);
			}
		}
	}
clearFields();
isCheked = false;
}

// переносим в доску, отображающую текущее состояние,
// необходимую расстановку шашек
function copyElem(array) {
board = array;
for (let i = 0; i < board.length; i++) {
	for (let j = 0; j < board.length; j++) {
			if (board[i][j] != "") {
				var id = characters[j] + (board.length - i);
				var elem = document.getElementById(id);
				var img = map.get(board[i][j]);
				map.delete(board[i][j]);
 				elem.appendChild(img);
			}
	}
}
}

//список всех обязательных и возможных полей текущей шашки
let redFields = new Array();
let greenFields = new Array();

// нажимаем на шашку
function clickCheker(id) {
changeCheker(id);
}

// осуществляем переход в режим подсказки и обратно
function changeCheker(id) {
var cheker = document.getElementById(id);

if (isBlack(cheker) || isWhite(cheker)) { // если шашка не выделена
	if (isCheked) { //и нет уже выделенной
	return;
	}
// то выделяем текущую  
	doCheking(cheker);
// ищем обязательные ходы у выделенной шашки и отмечаем их
	highlightRequired(id);
// если обязательных ходов не нашлось, то ищем возможные и их отмечаем
	if (redFields.length == 0) {
		unrequiredMove(id);
	}
// запоминаем, что уже выделили одну шашку
	isCheked = true;
} else if (isCheking(cheker)) { // если текущая шашка уже выделена
//делаем шашку не выделенной
	doUncheking(cheker, id);
// убираем все подсказки с полей
	clearFields();
// отмечаем, что больше нет выделенной шашки
	isCheked = false;
}
}

// данная фигура дамка?
function isKing(cheker) {
	return (cheker.src == blackKing || cheker.src == whiteKing || 
cheker.src == yellowKing);
}

//черная фигура?
function isBlack(cheker) {
	return cheker.src == blackKing || cheker.src == blackCheker;
}

// белая фигура?
function isWhite(cheker) {
	return cheker.src == whiteKing || cheker.src == whiteCheker;
}

// фигура в режиме подсказки?
function isCheking(cheker) {
	return cheker.src == yellowKing || cheker.src == yellowCheker;
}

// переводим шашку в режим подсказки
function doCheking(cheker) {
	if (isKing(cheker)) {
		cheker.src = yellowKing;
	} else {
		cheker.src = yellowCheker;
	}
}

// ищем все возможные ходы данной фигуры
function unrequiredMove(id) {
var cheker = document.getElementById(id);
var parentId = cheker.parentElement.id;

var row = 8 - parentId.charAt(1);
var col = getCol(parentId);

if (isKing(cheker)) {
	greenFieldsForKing(row, col);
} else {
	greenFieldsForCheker(row, col, id);
}

}

//ищем и выделяем все возможные ходы обычной шашки
//просматриваем все диагонали для хода вперёд
function greenFieldsForCheker(row, col, id) {
if (id[0] == 'b' && row < 7 && col > 0 && board[row + 1][col - 1] == "") {
	doGreen(col - 1, 7 - row);
}

if (id[0] == 'b' && col < 7 && row < 7 && board[row + 1][col + 1] == "") {
	doGreen(col + 1, 7 - row);
}

if (id[0] == 'w' && col > 0 && row > 0 && board[row - 1][col - 1] == "") {
	doGreen(col - 1, 9 - row);
}

if (id[0] == 'w' && col < 7 && row > 0 && board[row - 1][col + 1] == ""){
	doGreen(col + 1, 9 - row);
}

}

// просматриваем все диагонали у дамки и отмечаем все возможные ходы
function greenFieldsForKing(row, col) {
	topLeftGreenDiagonal(row, col);
	topRightGreenDiagonal(row, col);
	bottomLeftGreenDiagonal(row, col);
	bottomRightGreenDiagonal(row, col);
}


// смотрим обязательные ходы
function highlightRequired(id) {
var cheker = document.getElementById(id);
var parentId = cheker.parentElement.id;
var parent = document.getElementById(parentId);

var row = 8 - parentId.charAt(1);
var col = getCol(parentId);

//шашка для срубания
var item = '';
if (id[0] == 'b') {
	item = 'w';
} else {
	item = 'b';
}

if (isKing(cheker)) {
	redFieldsForKing(row, col, id, item);
} else {
	redFieldsForCheker(row, col, item);
}

}


//смотрим есть ли обязательные ходы у шашки
function redFieldsForCheker(row, col, item) {
if (col > 1 && row < 6 && board[row + 1][col - 1].charAt(0) == item && 
	board[row + 2][col - 2] == "") {
	doRed(col - 2, 6 - row);
}

if (col < 6 && row < 6 && board[row + 1][col + 1].charAt(0) == item &&
	board[row + 2][col + 2] == "") {
	doRed(col + 2, 6 - row);
}

if (col > 1 && row > 1 && board[row - 1][col - 1].charAt(0) == item &&
	board[row - 2][col - 2] == "") {
	doRed(col - 2, 10 - row);
}

if (col < 6 && row > 1 && board[row - 1][col + 1].charAt(0) == item &&
	board[row - 2][col + 2] == ""){
	doRed(col + 2, 10 - row);
}

}

//просматриваем все диагонали на наличие обязательных ходов у дамки
function redFieldsForKing(row, col, id, item) {
	topLeftDiagonal(row, col, id, item);
	topRightDiagonal(row, col, id, item);
	bottomLeftDiagonal(row, col, id, item);
	bottomRightDiagonal(row, col, id, item);
}

function topLeftDiagonal(row, col, id, item) {
if (col <= 1 || row <= 1) {
	return;
}
var curRow = row - 1;
var curCol = col - 1;
var elemTopLeft;
var isCutting = false;

//проверка по диагонали верхней левой
while (curCol >= 0 && curRow >= 0) {
	elemTopLeft = board[curRow][curCol];
	if (elemTopLeft[0] == id[0]) {
		return;
	}
	if (isCutting) {
		if (elemTopLeft != "") {
			curRow--;
			curCol--;
			continue;
		} 
		doRed(curCol, 8 - curRow);
	}
//срубаемый элемент на диагонале
	if (elemTopLeft[0] == item) {
		if (board[curRow - 1][curCol - 1] != "") {
			return;
		}
		isCutting = true;
	}
	curRow--;
	curCol--;
}
}

function topRightDiagonal(row, col, id, item) {
if (col >= 6 || row <= 1) {
	return;
}
var curRow = row - 1;
var curCol = col + 1;
var elemTopRight;
var isCutting = false;

//проверка по диагонали верхней правой
while (curCol <= 7 && curRow >= 0) {
	elemTopRight = board[curRow][curCol];

	if (elemTopRight[0] == id[0]) {
		return;
	}
	if (isCutting) {
		if (elemTopRight != "") {
			curRow--;
			curCol++;
			continue;
		} 
		doRed(curCol, 8 - curRow);
	}
//срубаемый элемент на диагонале
	if (elemTopRight[0] == item) {
		if (board[curRow - 1][curCol + 1] != "") {
			return;
		}
		isCutting = true;
	}
	curRow--;
	curCol++;
}
}

function bottomRightDiagonal(row, col, id, item) {
if (col >= 6 || row >= 6) {
	return;
}
var curRow = row + 1;
var curCol = col + 1;
var elemBottomRight;
var isCutting = false;

//проверка по диагонали нижняя правой
while (curCol <= 7 && curRow <= 7) {
	elemBottomRight = board[curRow][curCol];

	if (elemBottomRight[0] == id[0]) {
		return;
	}
	if (isCutting) {
		if (elemBottomRight != "") {
			curRow++;
			curCol++;
			continue;
		} 
		doRed(curCol, 8 - curRow);
	}
//срубаемый элемент на диагонале
	if (elemBottomRight[0] == item) {
		if (board[curRow + 1][curCol + 1] != "") {
			return;
		}
		isCutting = true;
	}
	curRow++;
	curCol++;
}
}

function bottomLeftDiagonal(row, col, id, item) {
if (col <= 1 || row >= 6) {
	return;
}
var curRow = row + 1;
var curCol = col - 1;
var elemBottomRight;
var isCutting = false;

//проверка по диагонали нижняя левая
while (curCol >= 0 && curRow <= 7) {
	elemBottomRight = board[curRow][curCol];
	if (elemBottomRight[0] == id[0]) {
		return;
	}
	if (isCutting) {
		if (elemBottomRight != "") {
			curRow++;
			curCol--;
			continue;
		} 
		doRed(curCol, 8 - curRow);
	}
//срубаемый элемент на диагонале
	if (elemBottomRight[0] == item) {
		if (board[curRow + 1][curCol - 1] != "") {
			return;
		}
		isCutting = true;
	}
	curRow++;
	curCol--;
}
}

function topLeftGreenDiagonal(row, col) {
if (col == 0 || row == 0) {
	return;
}
var curRow = row - 1;
var curCol = col - 1;
var elemTopLeft;
while (curCol >= 0 && curRow >= 0) {
	elemTopLeft = board[curRow][curCol];
	if (elemTopLeft != "") {
		return;
	}
	doGreen(curCol, 8 - curRow);
	curRow--;
	curCol--;
}
	
}

function topRightGreenDiagonal(row, col) {
if (col == 7 || row == 0) {
	return;
}
var curRow = row - 1;
var curCol = col + 1;
var elemTopRight;
while (curCol <= 7 && curRow >= 0) {
	elemTopRight = board[curRow][curCol];
	if (elemTopRight != "") {
		return;
	}
	doGreen(curCol, 8 - curRow);
	curRow--;
	curCol++;
}
}

function bottomLeftGreenDiagonal(row, col) {
if (col == 0 || row == 7) {
	return;
}
var curRow = row + 1;
var curCol = col - 1;
var elemTopRight;
while (curCol >=0 && curRow <= 7) {
	elemTopRight = board[curRow][curCol];
	if (elemTopRight != "") {
		return;
	}
	doGreen(curCol, 8 - curRow);
	curRow++;
	curCol--;
}
}

function bottomRightGreenDiagonal(row, col) {
if (col == 7 || row == 7) {
	return;
}
var curRow = row + 1;
var curCol = col + 1;
var elemTopRight;
while (curCol <= 7 && curRow <= 7) {
	elemTopRight = board[curRow][curCol];
	if (elemTopRight != "") {
		return;
	}
	doGreen(curCol, 8 - curRow);
	curRow++;
	curCol++;
}
}


//делаем обязательные поля красными
function doRed(col, row) {
	var itemId = characters[col] + (row);
	var item = document.getElementById(itemId);
	redFields.push(itemId);
	item.style.backgroundColor  = '#FF6347';
}

//делаем возможные поля зелёными
function doGreen(col, row) {
	var itemId = characters[col] + (row);
	var item = document.getElementById(itemId);
	greenFields.push(itemId);
	item.style.backgroundColor  = '#7FFFD4';
}

// получаем текущую букву на доске у шашки
function getCol(parentId) {
for (let i = 0; i < characters.length; i++) {
	if (characters[i] == parentId.charAt(0)) {
		return i;
	}
}
}

// убираем режим подсказки с шашки
function doUncheking(cheker, id) {
if (!isKing(cheker)) {
	if (id[0] == 'b') {
		cheker.src = blackCheker;	
	} else {
		cheker.src = whiteCheker;
	}
} else {
	if (id[0] == 'b') {
		cheker.src = blackKing;	
	} else {
		cheker.src = whiteKing;
	}
}
}

// убираем режим подсказки с ячеек доски
//и очищаем список выделенных полей
function clearFields() {
while (redFields.length > 0) {
	var item = document.getElementById(redFields.pop());
	item.style.backgroundColor = '#8B4513';
}

while (greenFields.length > 0) {
	var item = document.getElementById(greenFields.pop());
	item.style.backgroundColor = '#8B4513';
}
}
